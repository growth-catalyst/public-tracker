# Public Tracker

This is a public GitLab Project that aims to be all things GitLab and Datadog that can and should be shared with the open public. Open any private issues for discussions intended only with GitLab and Datadog.

# Overview

<!-- 

Use this section to detail the following information: 

-->

- **Value Proposition**: Value proposition of joint integration
- **Technical Overview**:  Technical overview of the joint integration. Can be a diagram, gif, or a link to a YouTube video explaining how the integration work. 
- **Joint Message**: 1 - 2 sentence marketing message of the joint integration / partnership 
- **Contacts** : Involved parties from the partner that helped bring this integration to market, their titles, and (optional) contact information if anyone wants to reach out and learn more. 
- **Proof Points**: Optional section for links to any reference case studies of your solution being leveraged by GitLab users. 
- **Docs** : Links to your documentation page for user reference
